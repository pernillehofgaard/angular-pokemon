export interface PokemonData{
  abilities?: any[];
  base_experience?: number;
  forms?: any;
  height: number;
  id?: number;
  moves?: any[];
  name: string;
  types?;
  sprites: ISprites;
  stats?: any[];
  weight: number;
  url: string;
  image?: string;
}

export interface ISprites{
  front_default: string;
  back_default: string;
  back_shiny: string;
  front_shiny: string;
}

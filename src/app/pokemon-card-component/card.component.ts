import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PokemonData } from 'src/models/pokemonData.model';

@Component({
  selector: 'app-card',
  templateUrl: 'card.component.html',
  styleUrls: ['card.component.scss']
})
export class CardComponent{

  @Input() pokemon: PokemonData;
  @Output() clickedOnPokemon: EventEmitter<number> = new EventEmitter();

  onCardClicked(): void{
    console.log(this.pokemon.id);
    this.clickedOnPokemon.emit( this.pokemon.id );
    
  }

}
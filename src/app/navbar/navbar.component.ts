import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TrainerService } from '../services/trainer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
})

export class NavbarComponent{

  constructor(private trainerService: TrainerService){
  }

  get trainerName(): Observable<string>{
    return this.trainerService.getTrainer();
  }
}
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PokemonData } from 'src/models/pokemonData.model';
import { PokemonDetailService } from '../../services/pokemon-detail.service';
import { ActivatedRoute } from '@angular/router';
import { TrainerCollectionService } from '../../services/trainer-collection.service'

@Component({
  selector: 'pokemon-detail',
  templateUrl: 'pokemon-detail.component.html',
  styleUrls: ['pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit{

  private pokemonId: number;
  private pokemon$: Subscription;
  pokemon: PokemonData;
  abilities: any[]= [];

  constructor(private pokemonDetailService: PokemonDetailService,
              private trainerCollectionService: TrainerCollectionService, 
              private route: ActivatedRoute){

    this.pokemonId = Number(this.route.snapshot.params.id);

    this.pokemon$ = this.pokemonDetailService.pokemon$.subscribe(pokemon => {
      this.pokemon = pokemon;
    });
  }

  get pokemonIsCollected(): Boolean{
    return this.trainerCollectionService.pokemonExistsInCollection( this.pokemonId );
  }

  
  ngOnInit():void{
    this.pokemonDetailService.fetchPokemon(this.pokemonId);
  }

  getAbilityUrls():string[]{
    return this.pokemon.abilities.map(({ability})=> ability.url)
  }

  getMoveUrls():string[]{
    return this.pokemon.moves.map(({move}) => move.url);
  }

  onCollectPokemonClicked(): void{
    this.trainerCollectionService.addPokemonToCollection(this.pokemon);
  }

  onRemovePokemonCliced(): void{
    this.trainerCollectionService.removePokemonFromCollection(this.pokemonId);
  }



}
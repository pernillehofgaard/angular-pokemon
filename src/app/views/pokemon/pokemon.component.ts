import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PokemonsService } from '../../services/pokemons.service';
import { PokemonData } from '../../../models/pokemonData.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['pokemon.component.scss']
})
export class PokemonComponent implements OnInit, OnDestroy{

  private pokemons$: Subscription;
  public pokemons: PokemonData[] = [];
  error: string = '';

  constructor(private pokemonService: PokemonsService, private router: Router){
    this.pokemons$ = this.pokemonService.pokemons$.subscribe((pokemons: PokemonData[]) => {
      this.pokemons = pokemons;
    })
    
  }
  getPokemon(): PokemonData[]{
    return this.pokemons.filter( pokemon =>{
      //console.log(pokemon.name);
      return pokemon.name.toLowerCase();
    });
  }

  ngOnInit(){
    this.pokemonService.fetchPokemonData();
    
  }
  
  ngOnDestroy(){
    this.pokemons$.unsubscribe();
  }
  
  get filteredImages(){
    console.log(this.pokemons);
    return this.pokemons.slice(0,10);
  }
  
  clickedOnPokemon(pokemonId): void{
    this.router.navigateByUrl(`/pokemon/${pokemonId}`)
    console.log("Clicked on pokemonId: ")
    console.log(pokemonId)
  }

  nextPage():void{
    this.pokemonService.next();
  }
  
  previousPage():void{
    this.pokemonService.prev();
  }
  
  
}
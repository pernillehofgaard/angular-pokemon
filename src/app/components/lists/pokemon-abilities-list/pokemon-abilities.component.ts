import { Component, Input, OnInit } from '@angular/core';
import { PokemonAbilityService } from '../../../services/pokemon-ability.service';

@Component({
  selector: 'app-pokemon-abilities',
  templateUrl:'pokemon-abilities.component.html',
  styleUrls: ['pokemon-abilities.component.scss']
})
export class PokemonAbilitiesComponent implements OnInit{
  @Input() abilityUrls: string[];
  abilities: any[] = [];
  error: string;

  constructor(private pokemonAbilityService: PokemonAbilityService){
  }

  async ngOnInit():Promise<any>{
    return await this.getAbilities();
  }

  public async getAbilities(): Promise<any>{
    this.abilities = [];
    try{
      this.abilities= await this.pokemonAbilityService.getDetails(this.abilityUrls);
    }catch(err){
      this.error = err.message;
    }
    return true;
  };
}
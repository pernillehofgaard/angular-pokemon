import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TrainerCollectionService } from 'src/app/services/trainer-collection.service';
import { PokemonData } from 'src/models/pokemonData.model';

@Component({
  selector: 'app-trainer-collection',
  templateUrl: 'pokemon-collection.html',
  styleUrls: ['pokemon-collection.scss']
})
export class PokemonCollection{
  constructor(private trainerCollectionService: TrainerCollectionService){
  }


  get collection$(): Observable<PokemonData[]>{
    console.log(this.trainerCollectionService.pokemonCollection);
    return this.trainerCollectionService.pokemonCollection;
  }

  clickedRemoveFromCollection(pokemonId: number){
    this.trainerCollectionService.removePokemonFromCollection(pokemonId);
  }

}
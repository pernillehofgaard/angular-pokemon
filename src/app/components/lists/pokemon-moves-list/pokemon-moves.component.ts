import { Component, OnInit, Input } from '@angular/core';
import { PokemonMoveService } from '../../../services/pokemon-moveset.service';

@Component({
  selector: 'app-pokemon-moves',
  templateUrl: 'pokemon-moves.component.html',
})
export class PokemonMovesComponent implements OnInit{
  @Input() movesUrls: string[];
  moves: any[] = [];
  error: string;

  constructor(private moveService: PokemonMoveService){
  }

  async ngOnInit(): Promise<any> {
    return await this.getMoveset();
  }

  public async getMoveset():Promise<any>{
    this.moves = [];
    try{
      this.moves = await this.moveService.getMoveSet(this.movesUrls);
    }catch(err){
      this.error = err.message;
    }
    return true;
  }
}
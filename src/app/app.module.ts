import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

//my components
import { HeaderComponent } from './header/header.component';
import { TrainerComponent } from './trainer-form/trainer.component';
import { CardComponent } from './pokemon-card-component/card.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterTrainerComponent } from './register-trainer.component/register-trainer.component';
import { PokemonComponent } from './views/pokemon/pokemon.component';
import { PokemonDetailComponent } from './views/pokemon-detail/pokemon-detail.component';
import { PokemonAbilitiesComponent } from './components/lists/pokemon-abilities-list/pokemon-abilities.component';
import { PokemonMovesComponent } from './components/lists/pokemon-moves-list/pokemon-moves.component';
import { PokemonCollection } from './components/lists/pokemon-collection/pokemon-collection';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TrainerComponent,
    PokemonComponent,
    PokemonDetailComponent,
    NavbarComponent,
    CardComponent,
    RegisterTrainerComponent,
    PokemonAbilitiesComponent,
    PokemonMovesComponent,
    PokemonCollection,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

}

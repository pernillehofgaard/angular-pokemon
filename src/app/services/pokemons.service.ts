import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PokemonData } from 'src/models/pokemonData.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PokemonsService {
  
  pokemons$: BehaviorSubject<PokemonData[]> = new BehaviorSubject([]);
  
  private pokemons: PokemonData[] = [];

  public offset = 0;
  public offsetEnd = 10;
  public pageLimit = 10;
  public pokemonLimit = 2000;
  public currentPage = 1;
  


  constructor(private http: HttpClient) { 
  }

  next(): void {
    this.offset += this.pageLimit;
    this.offsetEnd += this.pageLimit;
    this.pokemons$.next(this.pokemons.slice(this.offset, this.offsetEnd));
  }

  prev(){
    if(this.offset >= this.pageLimit){
      this.offset -= this.pageLimit;
      this.offsetEnd -= this.pageLimit;
      this.pokemons$.next(this.pokemons.slice(this.offset));
    }
    this.pokemons$.next(this.pokemons.slice(this.offset,this.offsetEnd));
  }
  
  url: string = `https://pokeapi.co/api/v2/pokemon?limit=${this.pokemonLimit}&offset=${this.offset}`
  
  getIdForImage(url:string):any{
    const id = url.split('/').filter(Boolean).pop();
    return{id, url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
  }
  public fetchPokemonData(): void{
    this.http.get<PokemonData[]> (this.url)
    .pipe(
      map( (response: any) => response.results.map(p => {
        return{
          ...p,
          ...this.getIdForImage(p.url)
        }
      }))
    ).subscribe(pokemons =>{
      this.pokemons = [...pokemons];
      this.pokemons$.next(this.pokemons.slice(this.offset,this.offsetEnd))
    });
  }
  
}

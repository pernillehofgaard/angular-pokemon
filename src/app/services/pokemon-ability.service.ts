import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Injectable({
  providedIn:'root'
})
export class PokemonAbilityService{
  constructor(private http: HttpClient){
  }

  getDetails(urls: string[]): Promise<any[]>{
    const ability= urls.map(url=>{
      return this.http.get(url)
      .toPromise()
      .then((result: any)=>{
        const {name, effect_entries } = result;
        return{
          name,
          effect_entries: effect_entries.filter(entry=>entry.language.name === 'en')
        };
      });
    });
    return Promise.all([...ability]);
  }
}


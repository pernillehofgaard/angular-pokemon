import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn:'root'
})
export class PokemonMoveService{
  constructor(private http: HttpClient){
  }

  getMoveSet(urls: string[]):Promise<any[]>{
    const moveset = urls.map(url=>{
      return this.http.get(url)
      .toPromise()
      .then((result: any) =>{
        const {name, effect_entries} = result;
        return{
          name,
          effect_entryies: effect_entries.filter(entry => entry.language==='en')
        };
      });
    });
    return Promise.all([...moveset]);
  }
}


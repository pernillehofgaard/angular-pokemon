import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs';
import { PokemonData } from '../../models/pokemonData.model';

const STORAGE_KEY = 'trainerCollection'

@Injectable({
  providedIn: 'root'
})
export class TrainerCollectionService{

  public pokemonCollection: BehaviorSubject<PokemonData[]> = new BehaviorSubject( [] );
  
  constructor(){
    this.start();
  }

  private start():void{
    const savedPokemons = localStorage.getItem(STORAGE_KEY);
    if(savedPokemons){
      this.pokemonCollection.next(JSON.parse(savedPokemons))
    }
  }

  public addPokemonToCollection(pokemonToAdd: PokemonData): boolean{
    const collection = [...this.pokemonCollection.value]
    if(this.pokemonExistsInCollection(pokemonToAdd.id)){
      return false;
    }
    collection.push(pokemonToAdd);
    this.pokemonCollection.next(collection);
    this.save()
    return true;
  }

  public removePokemonFromCollection(pokemonId: number): void{
    const collection = [...this.pokemonCollection.value];
    const updatedCollection = collection.filter((pokemon: PokemonData)=>{
      return pokemon.id !== pokemonId
    });
    this.pokemonCollection.next(updatedCollection);
    this.save()
  }

  public save(): void{
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.pokemonCollection.value));
  }

  public pokemonExistsInCollection(pokemonId):boolean{
    return Boolean(this.pokemonCollection.value.find((pokemon: PokemonData)=>{
      return pokemon.id === pokemonId;
    }))

  }

  
}


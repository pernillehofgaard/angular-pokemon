import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { PokemonData } from 'src/models/pokemonData.model';

@Injectable({
  providedIn:'root'
})
export class PokemonDetailService{
  
  pokemon$: BehaviorSubject<PokemonData> = new BehaviorSubject(null);

  constructor(private http: HttpClient){
  }

  fetchPokemon(id): void{
    this.pokemon$.next(null);

    this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}`).subscribe((pokemon: PokemonData)=>{
      this.pokemon$.next(pokemon);
    })
  }


 
}
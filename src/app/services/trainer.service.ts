import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

const STORAGE_KEY = 'loggedTrainerName';

@Injectable({
  providedIn: 'root'
})
export class TrainerService{

  private trainer$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(){
    const savedTrainerName = localStorage.getItem(STORAGE_KEY);
    if(savedTrainerName){
      this.trainer$.next(savedTrainerName);
    }
  }

  storeTrainerName(name: string):void{
    this.trainer$.next(name);
    localStorage.setItem(STORAGE_KEY, name);
  }

  trainerExists(): boolean{
    return Boolean(this.trainer$.value);
  }

  getTrainer(): Observable<string>{
    return this.trainer$.asObservable();
  }

}
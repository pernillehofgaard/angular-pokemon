import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { TrainerService } from '../services/trainer.service';

@Component({
  selector: 'app-trainer-form',
  templateUrl: 'trainer.component.html',
  styleUrls: ['trainer.component.scss']
})
export class TrainerComponent{

  @Output() registered: EventEmitter<void> = new EventEmitter();

  registerTrainerForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
    ])
  });
  
  constructor(private trainerService: TrainerService){
  }

  public get trainerName():AbstractControl{
    return this.registerTrainerForm.get('trainerName');
  }

  public clickedConfirm(){
    console.log("prøver å registrere")
    console.log(this.trainerName.value)
    this.trainerService.storeTrainerName(this.trainerName.value.trim()); //Trim value so user name can't be space-space-space
    this.registered.emit();
  }


}
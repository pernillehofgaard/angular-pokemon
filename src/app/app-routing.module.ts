
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//components
import { PokemonComponent } from './views/pokemon/pokemon.component';
import { PokemonDetailComponent } from './views/pokemon-detail/pokemon-detail.component';
import { RegisterTrainerComponent } from './register-trainer.component/register-trainer.component';
import { PokemonCollection } from './components/lists/pokemon-collection/pokemon-collection';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterTrainerComponent
  },
  {
    path: 'pokemon',
    component: PokemonComponent
  },
  {
    path: 'pokemon/:id',
    component: PokemonDetailComponent
  },
  {
    path: 'collection',
    component: PokemonCollection
  },
  {
    path: '**',
    component: RegisterTrainerComponent
  }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule],
})
export class AppRoutingModule{
}


import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TrainerService } from '../services/trainer.service';

@Component({
  selector: 'app-register-trainer',
  templateUrl: './register-trainer.component.html',
})
export class RegisterTrainerComponent{


  constructor(private router: Router, private trainerService: TrainerService){
    if(this.trainerService.trainerExists()){
      this.router.navigateByUrl('/pokemon')
    }
  }

  async onRegisteredTrainer() :Promise<boolean>{
    return await this.router.navigateByUrl('/pokemon');    
  }
}
